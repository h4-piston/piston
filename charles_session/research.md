# Airbnb API Research

## Complete URL:

https://api.airbnb.com/v2/users/7771271?client_id=3092nxybyb0otqw18e8nh5nty&locale=es-ES&currency=EUR&_format=v1_legacy_show

## Endpoint:

https://api.airbnb.com/v2/users/

To retrieve users, you need the user id first and the API Key, nothing else

## Headers

* Host: "api.airbnb.com"
* User-Agent: => Tested from desktop Firefox 41 and Windows 8.1
* Accept: "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
* Accept-Encoding: "gzip, deflate"
* Connection: "keep-alive"

## HTTP Method

Get

## State Code of Request

200

## UserID in AirBnB?

7771271?

## GET Params

* client_id=3092nxybyb0otqw18e8nh5nty // IMPORTANT API Key
* locale=es-ES // i18n
* currency=EUR // test with USD
* _format=v1_legacy_show

## Response

* Json file: response.json